from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(
    crystalcoin=math.Object(
        PARENT=networks.nets['crystalcoin'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares
        SPREAD=30, # blocks
        IDENTIFIER='a9119741aa1e3ee4'.decode('hex'),
        PREFIX='c2b189e984107cbd'.decode('hex'),
        P2P_PORT=20693,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**22 - 1,
        PERSIST=False,
        WORKER_PORT=20692,
        BOOTSTRAP_ADDRS='crystal.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-crystal',
        VERSION_CHECK=lambda v: True,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
